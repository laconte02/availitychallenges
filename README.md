# Availity Homework Assignment - Backend #


### What is this repository for? ###

This repository is to maintain both the answers and code to Availity's Homework Assignment

### Question 1 ###

Though it may not seem like much to others, my proudest professional achievement was landing my first full-time position as a software developer.  For years, I had worked at a position where I was being taken advantage of with little to show for it in terms of professional development.  The week before deciding to put in my two-weeks resignation notice, I was let go from my company.  While that saved me from initiating a difficult conversation, it was a blow to my self-confidence as a professional.  I shook off the doubt and decided to push forward with my pre-determined plan to learn software development.  With renewed vigor, I enrolled in an online coding “bootcamp” which started off well enough, but eventually led to failure.  I could not keep up with the pace and learning style being implemented, and had to drop from the program.  Doubt crept in again, but I was determined.  As such, I enrolled in another bootcamp, this time with a different learning structure and focus.  After almost four months of intensive learning, I was offered a full-time role on the last day of the program.  I had been unemployed and failed myself over the previous year and a half, but with perseverance and a clear goal, I was able to pivot my entire career and start developing meaningful code in a fast paced environment.

### Quesiton 2 ###

One of the more interesting articles that I have read recently was posted by The New York Times, which discusses a “somewhat joyless and aimless feeling” people may be experiencing as the result of the pandemic.  The article defines this feeling as “languishing” and goes on to describe its similarities to other mental health issues and emotions some may be experiencing while bunkered at home.  The reason this article hit home for me was two-fold: it described very well how I felt for a span during the pandemic; and it was fascinating to learn that others were sharing this feeling.  The article also made me reconsider the power behind “naming” a condition. Whereas before I didn’t really see the benefits to pinpointing and naming an emotional state, the article explains well the power behind it.  I would strongly suggest this to anyone who may have had a few uphill battles or felt last at any point in the past year or so: https://www.nytimes.com/2021/04/19/well/mind/covid-mental-health-languishing.html?referringSource=articleShare

### Question 3 ###

Availity helps maintain the relationship between health care providers and their patients.  It helps doctors, hospitals, and other medical providers make the registration and payment processes easier for the patient.  Availity works behind-the-scenes as a software, so the payments and information shared electronically by your doctors or at the hospitals are quick and accurate, without you having to do too much.

### Coding Exercise 1 Notes ###

The solution to check LISP code accuracy requires example LISP code be entered as an argument in the main method.  As such, the program can be run from the command line with example LISP code entered as args[0].

### Coding Exercise 2 Notes ###

The solution to sort a main CSV data file requires two things to be updated to run: the file path of the source CSV file, and the file path of the destination directory for the individual CSV files.  To update these paths, please see the "Variables.java" class, and update the related String variables.